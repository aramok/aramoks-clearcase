# Change Log
All notable changes to the "aramoks-black" extension will be documented in this file.


## [Unreleased]
- Application icon added
- cleartool , checkout , checkin , undo checkout , make element ... added
- cleartool path check
- Context menu added
- Initial Commit
